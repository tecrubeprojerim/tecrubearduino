#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

// Wifi isim ve şifre aynı olmalı diğeriyle
const char *ssid = "ESPap";
const char *password = "12345678";

ESP8266WebServer server(80);

unsigned long LastAvailableTime = 0, 
              MaxTime=5000; // 5 saniye içinde iletişim olmazsa hata durumuna geçer
              
 /*The blue LED on the ESP-01 module is connected to GPIO1 
 (which is also the TXD pin; so we cannot use Serial.print() at the same time)*/
//const int led = LED_BUILTIN;
const int output2 = 2;
long MyRssi=0;


void handleNotFound(){
  String MyUri = server.uri().substring(1,server.uri().length());
  MyRssi = MyUri.toInt();
  server.send(404, "text/plain", MyUri);
  LastAvailableTime=millis();
}
void setup() {
	delay(1000);// kaldırmadak için yokken test etmek lazım yada çok önemli değil dursun.
  pinMode(output2, OUTPUT);
  digitalWrite(output2,LOW);
	Serial.begin(115200);
	Serial.println();
	Serial.print("Configuring access point...");
  WiFi.mode(WIFI_AP_STA);
	/* You can remove the password parameter if you want the AP to be open. */
	WiFi.softAP(ssid, password);
	IPAddress myIP = WiFi.softAPIP(); //192.168.4.1 olur. varsayılan olarak WiFi.configle değştirilebilir istenirse
	Serial.print("AP IP address: ");
	Serial.println(myIP);
  server.onNotFound(handleNotFound);
	server.begin();
	Serial.println("HTTP server started");
}
unsigned long timeout=0;
void loop() {
  //yakalama için bu foksiyon burada olmalı
	server.handleClient();
  //Max zaman aşm kontrolü
  timeout=millis()-LastAvailableTime;
  Serial.print("myrrsi:");
  Serial.print(MyRssi);
  Serial.print(" time:");
  Serial.println(timeout);
  if(timeout  < MaxTime && -1>MyRssi && MyRssi>-70 )
  {
    //Serial.println("off");
    digitalWrite(output2, LOW);
  }
  else
  {
    //Serial.println("on");//üründe kaldırılabilir
    digitalWrite(output2, HIGH);
  }
}
