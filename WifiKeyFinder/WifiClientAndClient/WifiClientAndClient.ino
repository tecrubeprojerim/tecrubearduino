#include <ESP8266WiFi.h>

// Wifi isim ve şifre aynı olmalı diğeriyle
const char* ssid = "ESPap";
const char* password = "12345678";

//ip adresi varsayılan olarak bu adres atanıyor server'a
const char* host = "192.168.4.1";
IPAddress ip(192,168,4,1);
const int httpsPort = 80;
WiFiClient client;
long MyRssi=0;


unsigned long LastAvailableTime = 0, 
              MaxTime=5000; // 5 saniye içinde iletişim olmazsa hata durumuna geçer
//const int led = LED_BUILTIN;
const int output2 = 2;

void setup() {
  pinMode(output2, OUTPUT);
  digitalWrite(output2,LOW);
  Serial.begin(115200);
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
}
unsigned long timeout=0;
void loop() {
  
  if (WiFi.status() != WL_CONNECTED) {
    //delay(500);
    //Serial.print(".");
  }
  else
  {
    // Serial.print("connecting to ");
    //Serial.println(host);
    if (!client.connect(ip, httpsPort)) {
      //Serial.println("connection failed");
      return;
    }
    
    MyRssi = WiFi.RSSI(); 
    //Serial.println("connected");
    String url = "/"+String(MyRssi);
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "User-Agent: BuildFailureDetectorESP8266\r\n" +
                 "Connection: close\r\n\r\n");
    
    //Serial.println("request sent");
    /*while (client.connected()) {
      String line = client.readStringUntil('\n');
      if (line == "\r") {
        //Serial.println("headers received");
        break;
      }
    }
    String line = client.readStringUntil('\n');*/
    // reuest gerçekleşti ve cevap geldi
    LastAvailableTime=millis();
  }
  //Max zaman aşm kontrolü    //  0 -65 > -70 istenen durum
  timeout=millis()-LastAvailableTime;
  Serial.print("myrrsi:");
  Serial.print(MyRssi);
  Serial.print(" time:");
  Serial.println(timeout);
  if(timeout  < MaxTime && -1>MyRssi && MyRssi>-70 )
  {
    //Serial.println("off");
    digitalWrite(output2, LOW);
  }
  else
  {
    digitalWrite(output2, HIGH);
  }
}
