#define vcc 9
#define trigPin 10
#define echoPin 11
#define gnd 12
#define led 13

void setup() {
  Serial.begin (9600);
  
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(gnd, OUTPUT);
  pinMode(vcc, OUTPUT);
  pinMode(led, OUTPUT);
  
  digitalWrite(led,LOW);
  digitalWrite(gnd,LOW);
  digitalWrite(vcc,HIGH);
}

void loop() {
  long duration, distance;
  digitalWrite(trigPin, LOW);  
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW);
  //pulsein o pinin high olması ile saymaya başlar ve high kaldığı süreyi döndürür
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;
  //Mesafeyi yazdır
  Serial.print(distance);
  Serial.println(" cm");
  
  // This is where the LED On/Off happens
  if (distance <= 5) {  
    digitalWrite(led,HIGH); 
  }
  else {
    digitalWrite(led,LOW);
  }
  //bekleme sıklığı için ayarla. stabil oluğu en minimum seviye denenebilir.
  delay(100);
} 
